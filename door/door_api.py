import time
import json
import requests
from requestor import APIRequestor

class DoorAPI:

    api_base = None
    api_key = None
    
    #def __init__(self):
        

    def verifyTag(self, params):
        url_verify = "/verify"
        return self.do_request(url_verify, params, "post")
        
        
    def do_request(self, url=None, params=None, method="get", no_api_key=False):
        requestor = APIRequestor(self.api_key)
        params = self.dict_params(no_api_key, params)
        url = self.api_url(url)
        return requestor.request(url, params, method)
        
    def dict_params(self, no_api_key, params):
        api_key_param = {'api_key': self.api_key}

        if no_api_key:
            return params

        if params:
            return dict(api_key_param, **params)

        return api_key_param
        
    def api_url(self, url=''):
        return '%s%s' % (self.api_base, url)