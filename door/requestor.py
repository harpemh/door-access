import ast
import json
import platform
import textwrap
import requests
from exception import APIConnectionError, APIError, InvalidRequestError, AuthenticationError

class APIRequestor(object):
    def __init__(self, api_key):
        self.api_key = api_key

    def get_headers(self):
        ua = {
            'bindings_version': '1.0.0.0',
            'lang': 'python',
            'publisher': 'door_access',
            'httplib': 'requests',
        }

        for attr, func in [['lang_version', platform.python_version],
                           ['platform', platform.platform],
                           ['uname', lambda: ' '.join(platform.uname())]]:
            try:
                val = func()
            except Exception, e:
                val = "!! %s" % e
            ua[attr] = val

        headers = {
            'X-Door-Access-Client-User-Agent': json.dumps(ua),
            'User-Agent': 'Door-Access/v1 PythonBindings/%s' % '1.0.0.0',
        }

        return headers

    def request(self, url, params, method='get'):
        headers = self.get_headers()
        response = None
        try:
            try:
                if method == 'get':
                    response = requests.get(url, headers=headers, params=params)
                elif method == 'post':
                    response = requests.post(url, headers=headers, data=params)
            except TypeError, e:
                raise TypeError(
                    'Warning: It looks like your installed version of the "requests" library is not compatible '
                    'with door_access\'s usage thereof. (HINT: The most likely cause is '
                    'that your "requests" library is out of date. '
                    'You can fix that by running "pip install -U requests".) The underlying error was: %s' % (
                        e, ))

            content = response.content
            status_code = response.status_code

            return self.interpret_response(content, status_code)

        except ValueError:
            print "request error:", response.text
            # raise Exception(err)
        except Exception as e:
            self.handle_requests_error(e)

    def interpret_response(self, rbody, rcode):
        try:
            resp = json.loads(rbody.decode('utf-8'))
            if type(resp) == unicode:
                resp = ast.literal_eval(resp)

        except Exception:
            raise APIError("Invalid response body from API: %s (HTTP response code was %d)" % (rbody, rcode), rbody,
                           rcode)
        if not (200 <= rcode < 300):
            self.handle_api_error(rbody, rcode, resp)
        return resp

    def handle_api_error(self, rbody, rcode, resp):
        try:
            error = resp['error']
        except (KeyError, TypeError):
            raise APIError("Invalid response object from API: %r (HTTP response code was %d)" % (rbody, rcode), rbody,
                           rcode, resp)

        if rcode in [400, 404]:
            raise InvalidRequestError(error, rbody, rcode, resp)
        elif rcode == 401:
            raise AuthenticationError(error, rbody, rcode, resp)
        else:
            raise APIError(error, rbody, rcode, resp)

    def handle_requests_error(self, e):
        if isinstance(e, requests.exceptions.RequestException):
            msg = "Unexpected error communicating with Door Access.  "
            err = "%s: %s" % (type(e).__name__, str(e))
        else:
            msg = "Unexpected error communicating with Door Access.  " \
                  "It looks like there's probably a configuration issue locally.  "
            err = "A %s was raised" % (type(e).__name__, )
            if str(e):
                err += " with error message %s" % (str(e), )
            else:
                err += " with no error message"
        msg = textwrap.fill(msg) + "\n\n(Network error: " + err + ")"
        raise APIConnectionError(msg)