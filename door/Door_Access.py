from door_api import DoorAPI
import nfc
import sys
import time

def main():
    access = DoorAPI()
    
    access.api_key = ''
    access.api_base = ''
    
    running = True
    
    while running:
        time.sleep(5)
        cardId = read()
        try:
            accessed = access.verifyTag({"tag_id" : cardId})
            
            print(accessed['message'])
            
            if accessed['verified']:
                unlock()
                time.sleep(10)
                lock()
            else:
                lock()
        except Exception as e: 
            print(e)
    
def unlock():
    print("Unlock")
    
def lock():
    print("Lock")
 
def read():
    #ledRedOn()
    cardId=nfc.readNfc()
    #ledRedOff()
    return cardId    

if __name__ == "__main__":
    main()
