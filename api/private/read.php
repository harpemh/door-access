<?php

require_once('database.php');
date_default_timezone_set('America/New_York');

class Read {



    public function verifyTag($tagID) {
        $data = $this->getTagData($tagID);
        
        $verify = false;
        
        $verify = ($this->isCard($data));
        $verify = ($verify and $this->isActive($data));
        if ($verify)
            $verify = $this->isAllowedTime($data[0]["PEOPLE_ID"]);
            
        return $verify;
    }
    
    private function isCard($data) {
        return isset($data) and count($data) == 1;
    }
    
    private function isActive($data) {
        return ($data[0]["CARD_ACTIVE"] and !$data[0]["CARD_RETIRED"] and $data[0]["PERSON_ACTIVE"]);
    }
    
    private function getTagData($tagID) {
        return $this->select("Select tags.TAG_ID, tags.CODE, tags.ACTIVE as 'CARD_ACTIVE', tags.RETIRED as 'CARD_RETIRED', people.PEOPLE_ID, people.FULL_NAME, people.CARD_ID, people.ACTIVE as 'PERSON_ACTIVE' from tags inner join people on tags.TAG_ID = people.CARD_ID where tags.CODE = ?", array($tagID));
    }
    
    private function isAllowedTime($personID) {
        $data = $this->select("Select * from allowed_times where USER_ID = ? and ACTIVE = 1", array($personID));
        
        if (count($data) == 0) return false;
        
        
        
        $verify = false;
        foreach ($data as &$allTime)
        {
            $dtStart = new DateTime($allTime["START"]);
            $dtEnd = new DateTime($allTime["END"]);
            $dtNow = new DateTime('NOW');
        
            // Once between dates
            if ($allTime["FREQUENCY"] == 1 and ($dtStart <= $dtNow and $dtNow <= $dtEnd))
                return true;
            // Daily
            if ($allTime["FREQUENCY"] == 4 and (($dtStart <= $dtNow and $dtEnd >= $dtNow)) and 
                ($this->dateDiff($dtNow, $dtStart, '%a') % $allTime["INTERVAL"] == 0) and (time() >= strtotime($dtStart->format('H:i:s')) and time() <= strtotime($dtEnd->format('H:i:s'))))
                return true;
            // Weekly
            if ($allTime["FREQUENCY"] == 8 and (($dtStart <= $dtNow and $dtEnd >= $dtNow)) and ($this->checkWeekDay($dtNow->format("w"), decbin($allTime["INTERVAL"]))) and
				(($this->dateDiff($dtNow, $dtStart, '%a') / 7) % ($allTime["FREQUENCY_FACTOR"]) == 0) and (time() >= strtotime($dtStart->format('H:i:s')) and time() <= strtotime($dtEnd->format('H:i:s'))))
				return true;
			//Monthly
            if ($allTime["FREQUENCY"] == 16 and ($dtStart <= $dtNow and $dtEnd >= $dtNow) and ($this->checkMonthDay($dtNow, $allTime["INTERVAL"])) and
				((($this->dateDiff($dtNow, $dtStart, '%y') * 12) + $this->dateDiff($dtNow, $dtStart, '%m')) % ($allTime["FREQUENCY_FACTOR"]) == 0) and (time() >= strtotime($dtStart->format('H:i:s')) and time() <= strtotime($dtEnd->format('H:i:s'))))
				return true;
        }
       
        return $verify;
        
    }
	
	private function checkMonthDay($curdate, $interval)
	{
		$day = $curdate->format("j");
		$numDays = $curdate->format("t");
		
		return (($interval == $day) or ($interval >= $numDays and $numDays == $day));
	}
	
	private function checkWeekDay($day, $binary)
	{
		$reverse = strrev($binary) . str_repeat("0", 7);
		return ($reverse[$day] == "1");
	}
	
	private function dateDiff($date1, $date2, $format)
	{
		$date1->setTime(0,0);
		$date2->setTime(0,0);
		
		return $date1->diff($date2)->format($format);
	}
    
    private function getDateTime($date = null, $time = null) {
        if ($date != null and $time == null)
            return new DateTime($date);
        if ($date != null and $time != null)
            return new DateTime($date . ' ' . $time);
            
        return null;
    }
    
    private function select($query, $values) {
        $params = array(); 
        foreach ($values as &$value) { 
            $params[] = &$value;
        }
      
        $data = array();
        $link = getHandler();
        
        if ($stmt = $link->prepare($query))
        {
            $types  = array(str_repeat('s', count($params))); 
            $values = array_merge($types, $params); 
            call_user_func_array(array($stmt, 'bind_param'), $values);
            $result = $stmt->execute();
            $result = $stmt->get_result();
        }
        while($row = mysqli_fetch_array($result)) {$data[] = $row;}
        
        return $data;
    }




}