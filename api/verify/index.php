<?php

require_once("../private/login.php");
require_once("../private/read.php");

$data = array();

if (!isset($_POST["api_key"]))
{
    $data["error"] = true;
    $data["verified"] = false;
    $data["result"] = "API Key is not set.";
    echo json_encode($data);
    return;
}
if (!isset($_POST["tag_id"]))
{
    $data["error"] = true;
    $data["verified"] = false;
    $data["result"] = "Tag ID was not passed.";
    echo json_encode($data);
    return;
}

$api_key = $_POST["api_key"];
$tag_id = $_POST["tag_id"];


if (!login($api_key))
{
    $data["error"] = true;
    $data["verified"] = false;
    $data["result"] = "API Key could not be verified.";
    echo json_encode($data);
    return;
}

$reading = new Read();
$verified = $reading->verifyTag($tag_id);

if (!$verified)
{
    $data["error"] = true;
    $data["verified"] = false;
    $data["result"] = "Could not verify key.";
    echo json_encode($data);
    return;
}
else
{
    $data["error"] = false;
    $data["verified"] = true;
    $data["result"] = "SUCCESS";
    echo json_encode($data);
    return;
}


